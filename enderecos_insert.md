# Dados que devem ser inseridos na tabela **`enderecos`**

|rua|pais|cidade|
|----|:---|:---|
|Avenida Higienópolis|Brasil|Londrina|
|Avenida Paulista|Brasil|São Paulo|
|Rua Marcelino Champagnat|Brasil|Curitiba|

<br/><br/>

# Dados que devem ser inseridos na tabela **`usuarios`**

|nome|email|senha|endereco_id|
|----|:----|:----|:----------|
|Cauan|cauan@exemple.com|1234|id da rua parecida com 'Paulista' e igual a 'São Paulo'|
|Chrystian|chrystian@exemple.com|4321|id da rua parecida com 'Marcelino' e igual a 'Curitiba'|
|Matheus|matheus@exemple.com|3214|id darua parecida com 'Higienópolis' e igual a 'Londrina'|
 
<br/><br/>

# Dados que devem ser inseridos na tabela **`redes_sociais`**

|nome|
|----|
|Youtube|
|Twitter|
|Instagram|
|Facebook|
|TikTok|

<br/><br/>

# Dados que devem ser inseridos na tabela **`usuario_redes_sociais`**

|usuario_id|rede_social_id|
|----|:---|
|id do usuario com o nome 'Cauan'|id da rede social 'Youtube'|
|id do usuario com o nome 'Chrystian'|id da rede social 'Youtube'|
|id do usuario com o nome 'Matheus'|id da rede social 'Youtube'|
|id do usuario com o nome 'Chrystian'|id da rede social 'Twitter'|
|id do usuario com o nome 'Cauan'|id da rede social 'Twitter'|
|id do usuario com o nome 'Matheus'|id da rede social 'Instagram'|
|id do usuario com o nome 'Matheus'|id da rede social 'Facebook'|
|id do usuario com o nome 'Chrystian'|id da rede social 'Instagram'|
|id do usuario com o nome 'Cauan'|id da rede social 'TikTok'|
