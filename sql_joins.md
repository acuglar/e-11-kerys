# Primeiro SELECT

## Retorno da seleção de tudo da tabela **`enderecos`**.

| id  | rua                      | pais   | cidade    |
| --- | :----------------------- | :----- | :-------- |
| 1   | Avenida Higienópolis     | Brasil | Londrina  |
| 2   | Avenida Paulista         | Brasil | São Paulo |
| 3   | Rua Marcelino Champagnat | Brasil | Curitiba  |

<br/><br/>

# Segundo SELECT

## Retorno da seleção de tudo das tabelas **`enderecos`** e **`usuarios` odernado pelo id de enderecos UTILIZANDO JOINS**.

| id  | rua                      | pais   | cidade    | id  | nome      | email                 | senha | endereco_id |
| --- | :----------------------- | :----- | :-------- | :-- | :-------- | :-------------------- | :---- | :---------- |
| 1   | Avenida Higienópolis     | Brasil | Londrina  | 3   | Matheus   | matheus@example.com   | 3214  | 1           |
| 2   | Avenida Paulista         | Brasil | São Paulo | 1   | Cauan     | cauan@example.com     | 1234  | 2           |
| 3   | Rua Marcelino Champagnat | Brasil | Curitiba  | 2   | Chrystian | chrystian@example.com | 4321  | 3           |

<br/><br/>

# Terceiro SELECT

## Retorno da seleção de tudo das tabela **`redes_sociais`** e **`usuarios` UTILIZANDO JOINS**.

| id  | nome      | id  | nome      | email                 | senha | endereco_id |
| --- | :-------- | :-- | :-------- | :-------------------- | :---- | :---------- |
| 1   | Youtube   | 1   | Cauan     | cauan@example.com     | 1234  | 2           |
| 1   | Youtube   | 2   | Chrystian | chrystian@example.com | 4321  | 3           |
| 1   | Youtube   | 3   | Matheus   | matheus@example.com   | 3214  | 1           |
| 2   | Twitter   | 2   | Chrystian | chrystian@example.com | 4321  | 3           |
| 2   | Twitter   | 1   | Cauan     | cauan@example.com     | 1234  | 2           |
| 3   | Instagram | 3   | Matheus   | matheus@example.com   | 3214  | 1           |
| 4   | Facebook  | 3   | Matheus   | matheus@example.com   | 3214  | 1           |
| 3   | Instagram | 2   | Chrystian | chrystian@example.com | 4321  | 3           |
| 5   | TikTok    | 1   | Cauan     | cauan@example.com     | 1234  | 2           |

<br/><br/>

# Quarto SELECT

## Retorno da seleção de tudo das tabela **`redes_sociais`**, **`usuarios`** e **`enderecos` UTILIZANDO JOINS**.

| id  | nome      | id  | nome      | email                 | senha | endereco_id | id  | rua                      | pais   | cidade    |
| --- | :-------- | :-- | :-------- | :-------------------- | :---- | :---------- | :-- | :----------------------- | :----- | :-------- |
| 1   | Youtube   | 1   | Cauan     | cauan@example.com     | 1234  | 2           | 2   | Avenida Paulista         | Brasil | São Paulo |
| 1   | Youtube   | 2   | Chrystian | chrystian@example.com | 4321  | 3           | 3   | Rua Marcelino Champagnat | Brasil | Curitiba  |
| 1   | Youtube   | 3   | Matheus   | matheus@example.com   | 3214  | 1           | 1   | Avenida Higienópolis     | Brasil | Londrina  |
| 2   | Twitter   | 2   | Chrystian | chrystian@example.com | 4321  | 3           | 3   | Rua Marcelino Champagnat | Brasil | Curitiba  |
| 2   | Twitter   | 1   | Cauan     | cauan@example.com     | 1234  | 2           | 2   | Avenida Paulista         | Brasil | São Paulo |
| 3   | Instagram | 3   | Matheus   | matheus@example.com   | 3214  | 1           | 1   | Avenida Higienópolis     | Brasil | Londrina  |
| 4   | Facebook  | 3   | Matheus   | matheus@example.com   | 3214  | 1           | 1   | Avenida Higienópolis     | Brasil | Londrina  |
| 3   | Instagram | 2   | Chrystian | chrystian@example.com | 4321  | 3           | 3   | Rua Marcelino Champagnat | Brasil | Curitiba  |
| 5   | TikTok    | 1   | Cauan     | cauan@example.com     | 1234  | 2           | 2   | Avenida Paulista         | Brasil | São Paulo |

<br/><br/>

# Quinto SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS**.

| rede_social | usuario   | email                 | cidade    |
| ----------- | :-------- | :-------------------- | :-------- |
| Youtube     | Cauan     | cauan@example.com     | São Paulo |
| Youtube     | Chrystian | chrystian@example.com | Curitiba  |
| Youtube     | Matheus   | matheus@example.com   | Londrina  |
| Twitter     | Chrystian | chrystian@example.com | Curitiba  |
| Twitter     | Cauan     | cauan@example.com     | São Paulo |
| Instagram   | Matheus   | matheus@example.com   | Londrina  |
| Facebook    | Matheus   | matheus@example.com   | Londrina  |
| Instagram   | Chrystian | chrystian@example.com | Curitiba  |
| TikTok      | Cauan     | cauan@example.com     | São Paulo |

<br/><br/>

# Sexto SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS** aonde a rede é Youtube.

| rede_social | usuario   | email                 | cidade    |
| ----------- | :-------- | :-------------------- | :-------- |
| Youtube     | Cauan     | cauan@example.com     | São Paulo |
| Youtube     | Chrystian | chrystian@example.com | Curitiba  |
| Youtube     | Matheus   | matheus@example.com   | Londrina  |

<br/><br/>

# Sétimo SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS** aonde a rede é Instagram.

| rede_social | usuario   | email                 | cidade   |
| ----------- | :-------- | :-------------------- | :------- |
| Instagram   | Matheus   | matheus@example.com   | Londrina |
| Instagram   | Chrystian | chrystian@example.com | Curitiba |

<br/><br/>

# Oitavo SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS** aonde a rede é Facebook.

| rede_social | usuario | email               | cidade   |
| ----------- | :------ | :------------------ | :------- |
| Facebook    | Matheus | matheus@example.com | Londrina |

<br/><br/>

# Nono SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS** aonde a rede é TikTok.

| rede_social | usuario | email             | cidade    |
| ----------- | :------ | :---------------- | :-------- |
| TikTok      | Cauan   | cauan@example.com | São Paulo |

<br/><br/>

# Décimo SELECT

## Retorno da seleção da coluna nome da tabela **`redes_sociais`**, da coluna nome e email da tabela **`usuarios`** e da coluna cidade da tabela **`enderecos` UTILIZANDO JOINS** aonde a rede é Twitter.

| rede_social | usuario   | email                 | cidade    |
| ----------- | :-------- | :-------------------- | :-------- |
| Twitter     | Chrystian | chrystian@example.com | Curitiba  |
| Twitter     | Cauan     | cauan@example.com     | São Paulo |
