SELECT * FROM enderecos;


SELECT 
	* 
FROM 
	enderecos e
JOIN
	usuarios u
	ON u.endereco_id = e.id
	ORDER BY e.id;


SELECT 
	rs redes_sociais, u usuarios
FROM 
	usuarios_redes_sociais urs
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id; 
	

SELECT 
	rs redes_sociais, u usuarios, e enderecos
FROM 
	usuarios_redes_sociais urs
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	enderecos e 
	ON e.id = u.endereco_id; 


SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs 
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id 
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id;


SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id 
WHERE rs.nome = 'Youtube';


SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id 
WHERE rs.nome = 'Instagram';
	

SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id 
WHERE rs.nome = 'Facebook';


SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id 
WHERE rs.nome = 'Tiktok';


SELECT 
	rs.nome rede_social, u.nome usuario, u.email, e.cidade
FROM 
	usuarios_redes_sociais urs
JOIN 
	redes_sociais rs 
	ON rs.id = urs.rede_social_id
JOIN 
	usuarios u 
	ON u.id = urs.usuario_id 
JOIN 
	enderecos e 
	ON e.id = u.endereco_id 
WHERE rs.nome = 'Twitter';
